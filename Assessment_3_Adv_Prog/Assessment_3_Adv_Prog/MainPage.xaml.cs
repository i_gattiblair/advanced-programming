﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using SetupSQLite;
using SQLite;
using Xamarin.Forms;

namespace Assessment_3_Adv_Prog
{
    public class WebURL
    {
        [SQLite.PrimaryKey, SQLite.AutoIncrement]
        public int ID { get; set; }
        public string URL { get; set; }
        public string ImageShow { get; set; }
        public string Title { get; set; }
    }

    public partial class MainPage : ContentPage
    {

        private SQLiteAsyncConnection _connection;
        private List<WebURL> urls;
        
        public MainPage()
        {
            InitializeComponent();

            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();

        }

        async void ReloadPicker()
        {
            await _connection.CreateTableAsync<WebURL>();
            urls = await _connection.Table<WebURL>().ToListAsync();


            if (ShowURlS.Items.Count != 0)
            {
                ShowURlS.Items.Clear();
            }

            foreach (var x in urls)
            {
                ShowURlS.Items.Add($"{x.title}");
            }
        }

        protected override void OnAppearing()
        {
            ReloadPicker();

            base.OnAppearing();
        }

        async void OpenWebView(object sender, System.EventArgs e)
        {
            var passOnUrl = urls[ShowURlS.SelectedIndex].URL;
            await Navigation.PushModalAsync(new WebViewPage(passOnUrl));
        }

        async void CreateLink(object sender, System.EventArgs e)
        {
            await Navigation.PushModalAsync(new CreatePage());
        }

        async void EditLink(object sender, System.EventArgs e)
        {
            urls = await _connection.Table<WebURL>().ToListAsync();
            var selectedItems = ShowURlS.SelectedIndex;
            var selectedClass = urls[ShowURlS.SelectedIndex];


            await Navigation.PushModalAsync(new EditingPage(selectedClass, selectedItems));
        }

        async void RemoveLink(object sender, System.EventArgs e)
        {
            urls = await _connection.Table<WebURL>().ToListAsync();
            var selectedItem = urls[ShowURlS.SelectedIndex];
            await _connection.DeleteAsync(selectedItem);

            ReloadPicker();
        }
    }
}
