﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;


namespace Assessment_3_Adv_Prog
{
        public partial class WebViewPage : ContentPage
    {
        public WebViewPage(string Url)
        {


            WebView webView = new WebView
            {
                Source = new UrlWebViewSource
                {
                    Url = ShowWeb.Source
                },
                VerticalOptions = LayoutOptions.FillAndExpand
            };

            // Accomodate iPhone status bar.
            this.Padding = new Thickness(10, Device.OnPlatform(20, 0, 0), 10, 5);

            // Build the page.
            this.Content = new StackLayout
            {
                Children =
                {
                    
                    webView
                }
            };
        }
    }
}
