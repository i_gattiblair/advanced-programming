﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using SetupSQLite;
using SQLite;
using Xamarin.Forms;

namespace Assessment_3_Adv_Prog
{
    public partial class CreatePage : ContentPage
    {
        private SQLiteAsyncConnection _connection;
        private List<WebURL> urls;

        public CreatePage()
        {
            InitializeComponent();
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
        }


        async void createUrl(object sender, EventArgs e)
        {

            var url = new WebURL { Title = TitleNew.Text, ImageShow = ImageNew.Text, URL = URLNew.Text };
            await _connection.InsertAsync(url);

            TitleNew.Text = "";
            ImageNew.Text = "";
            URLNew.Text = "";
        }

        async void BackToPreviousPage(object sender, System.EventArgs e)
        {

            await Navigation.PopModalAsync();
        }
    }
}
